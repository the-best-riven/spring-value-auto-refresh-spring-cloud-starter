package com.riven.config.springvaluerefresh;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.context.environment.EnvironmentChangeEvent;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.cloud.context.scope.refresh.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

import static org.springframework.beans.factory.config.BeanDefinition.ROLE_INFRASTRUCTURE;

/**
 * @author riven
 * @since 2021-04-12
 */
@Slf4j
@Configuration
@ConditionalOnClass({ContextRefresher.class, RefreshScope.class, EnvironmentChangeEvent.class})
public class SpringValueAutoRefreshConfiguration {

    public static final String PROCESSOR_BEAN_NAME = "com.riven.config.springvaluerefresh.SpringValueAutoRefreshProcessor";

    @Role(ROLE_INFRASTRUCTURE)
    @Bean(PROCESSOR_BEAN_NAME)
    public BeanPostProcessor springValueAutoRefreshProcessor() {
        return new SpringValueAutoRefreshProcessor();
    }

}
