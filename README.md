# spring-value-auto-refresh-spring-cloud-starter

#### 介绍
配置中心修改配置，实现springcloud应用@Value配置的自动刷新

#### 背景
研究nacos时发现，springboot版本可使用@NacosValue实现配置的自动刷新，spring原生注解@Value则无法自动刷新\
springcloud版本可采用两种方式（可能还有其他方式暂未研究过）实现自动刷新：\
1.手动注入@NacosValue注解的处理器并使用该注解修饰相关字段或方法，这需要弄清楚底层的来龙去脉，比较麻烦，且不支持spring原生@Value注解。\
2.借助@RefreshScope，将bean定义为RefreshScope。此方法也略显麻烦，每个存在配置需要刷新的类都要定义成RefreshScope。\
本项目就是为了解决上述问题而产生，引入依赖之后自动实现@Value配置的自动刷新，\
并且不是专门为nacos开发，只依赖springcloud的ContextRefresh机制（nacos的配置刷新也使用到了ContextRefresh）。

#### 软件架构
基于springcloud的ContextRefresh机制，监听EnvironmentChangeEvent事件并重新注入@Value配置\
需jdk1.8以上

#### 安装教程
##### 依赖引入
```xml
<dependency>
    <groupId>com.gitee.the-best-riven</groupId>
    <artifactId>spring-value-auto-refresh-spring-cloud-starter</artifactId>
    <version>1.0.0.RELEASE</version>
</dependency>
```
##### spring版本
默认为以下版本，可自定义覆盖
```xml
<spring.version>5.0.10.RELEASE</spring.version>
<spring-boot.version>2.0.6.RELEASE</spring-boot.version>
<spring-cloud.version>Finchley.RELEASE</spring-cloud.version>
```
#### 特别说明
由于本人水平有限，不一定是最好的实现方式，如有问题欢迎指正。
